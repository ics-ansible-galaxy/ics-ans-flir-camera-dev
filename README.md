# ics-ans-flir-camera-dev

Ansible playbook to setup a FLIR camera development environment.

This playbook currently installs:
 - Phoebus
 - ImageJ (including ADViewer plugin)
 - Spinnaker SDK

## License

BSD 2-clause
