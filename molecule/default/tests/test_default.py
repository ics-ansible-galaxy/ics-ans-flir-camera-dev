import os
import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("flir_camera_dev")

IMAGEJ_HOME = "/opt/ImageJ"


def test_phoebus_installed(host):
    cmd = host.run("/usr/local/bin/phoebus -help")
    assert cmd.rc == 0


@pytest.mark.parametrize(
    "plugin",
    [
        "EPICS_areaDetector/EPICS_AD_Viewer.java",
        "EPICS_areaDetector/EPICS_NTNDA_Viewer.java",
    ],
)
def test_imagej_plugins_installed(host, plugin):
    assert host.file(os.path.join(IMAGEJ_HOME, "plugins", plugin)).exists


@pytest.mark.parametrize("package", ["spinnaker"])
def test_spinnaker_installed(host, package):
    assert host.package(package).is_installed


@pytest.mark.parametrize("package", ["flycap"])
def test_flycap_uninstalled(host, package):
    assert not host.package(package).is_installed
